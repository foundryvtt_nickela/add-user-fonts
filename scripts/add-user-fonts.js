/*
TO ADD IN A CUSTOM FONT...
Go to https://https://fonts.google.com/
Find a font you want to use.
Copy and paste the two lines of code below at the bottom of this file, replacing myFont with the name of the desired font.
----

FONTS["myFont"] = {custom: {families: ["myFont"], urls: ["https://fonts.googleapis.com/css?family=myFont"]}}
loadFont("MyFont");

-----
Load the module in your world and now you have the fonts below as choices with the drawing tool!
Happy Fonting!
*/

FONTS["Aladin"] = {custom: {families: ["Aladin"], urls: ["https://fonts.googleapis.com/css?family=Aladin"]}}
loadFont("Aladin");

FONTS["Iceberg"] = {custom: {families: ["Iceberg"], urls: ["https://fonts.googleapis.com/css?family=Iceberg"]}}
loadFont("Iceberg");

FONTS["Kaushan Script"] = {custom: {families: ["Kaushan Script"], urls: ["https://fonts.googleapis.com/css?family=Kaushan Script"]}}
loadFont("Kaushan Script");
