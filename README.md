# Add User Fonts
Add custom fonts from Google Fonts to be used with Foundry's drawing tools.
## Installation Instructions
To install a module, follow these instructions:

1. From the Foundry setup page, go to the Add-on Modules page.
2. Click on the Install Module button.
3. Paste the manifest url for the module.json into the install module option and press install.

[https://gitlab.com/foundryvtt_nickela/add-user-fonts/raw/master/module.json](https://gitlab.com/foundryvtt_nickela/add-user-fonts/raw/master/module.json)

## Adding a new custom font

1. Go to https://https://fonts.google.com/ to find a font you'd like to add.
2. Find and open the script file in your Foundry data directory. It default location should be /Data/modules/add-user-fonts/scripts/add-user-fonts.js
3. Copy and paste the two lines of code below at the bottom of the script file, replacing **myFont** with the name of the desired font.

----

```
FONTS["myFont"] = {custom: {families: ['myFont'], urls: ['https://fonts.googleapis.com/css?family=myFont']}}
loadFont('MyFont');
```
-----

Enable the module in your world and now you have the added fonts as choices with the drawing tool!
Happy Fonting!

# A note on updating

Since adding in new fonts edits the script file and updating the module would overwrite it, you should manually update minimumCoreVersion in the module.json so that it loads correctly after Foundry updates to a new version. I'll keep it up to date for new downloads, but I'm leaving it at module version at 1.0 so you don't accidentally overwrite your added fonts.
